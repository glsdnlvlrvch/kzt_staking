//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./ERC-20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "hardhat/console.sol";


contract HalykBankStaking is AccessControl {
    uint256 private _freezeTime;
    uint256 private _rewardTime;
    uint256 private _percent;
    address private _owner;

    address private _rewardToken; // KZT
    address private _stakingToken; // VRT\LP

    bytes32 private constant ADMIN = keccak256("ADMIN");

    struct Contributor{
        uint256 deposit;
        uint256 startDate;
        uint256 reward;
        bool isValue;   // check if key was set
    }

    mapping (address => Contributor) private contributors;

    constructor(address token1, address token2, uint256 frzTime, uint256 rwdTime) {
        _freezeTime = frzTime;
        _percent = 5; // percents
        _rewardTime = rwdTime;

        _owner = msg.sender;
        _setupRole(ADMIN, _owner);

        _rewardToken = token1; // KZT
        _stakingToken = token2; // VRT\LP
    }

    function _calculateReward(Contributor storage _contributor) internal returns(uint256) {
        uint256 coefficient = (block.timestamp - _contributor.startDate) / _rewardTime;

        for (uint256 i = 0; i < coefficient; i++) {
            _contributor.reward += _contributor.deposit * (_percent * 100) / 10000;
        }

        return _contributor.reward;
    }

    // Functions for contributors
    function stake(uint256 _value) public returns (bool success) {
        require(
            IERC20(_stakingToken).balanceOf(msg.sender) >= _value,
            "not enough LP"
        );

        IERC20(_stakingToken).transferFrom(msg.sender, address(this), _value);

        Contributor storage contributor = contributors[msg.sender];
        if(contributors[msg.sender].isValue == true) {
            contributor.reward += _calculateReward(contributor);
            contributor.deposit += _value;
            contributor.startDate = block.timestamp;
        }
        else{
            contributors[msg.sender] = Contributor(_value, block.timestamp, 0, true);
        }

        return true;
    }

    function claim() public returns (bool success) {
        Contributor storage contributor = contributors[msg.sender];
        require(
            block.timestamp - contributor.startDate > _freezeTime,
            "try later"
        );

        uint256 reward = _calculateReward(contributor);
        IERC20(_rewardToken).transfer(msg.sender, reward);

        contributor.reward = 0;
        contributor.startDate = block.timestamp;
        return true;
    }

    function unstake() public returns (bool success) {
        Contributor storage contributor = contributors[msg.sender];
        require(
            contributor.deposit > 0,
            "nothing to unstake"
        );

        IERC20(_stakingToken).transfer(msg.sender, contributor.deposit);

        return true;
    }

    // Getter functions
    function freezeTime() public view returns (uint256) {
        return _freezeTime;
    }

    function percent() public view returns (uint256) {
        return _percent;
    }

    function getContributorReward(address _sender) public view returns (uint256) {
        return contributors[_sender].reward;
    }

    function getContributorDeposit(address _sender) public view returns (uint256) {
        return contributors[_sender].deposit;
    }
    
    // Functions for administator
    function setFreezeTime(uint256 _value) public onlyRole(ADMIN)  returns (bool success) {
        require(
            _value <= 1 hours,
            "Freeze time must be <= 1 hour"
        );
        require(
            _value >= 5 seconds,
            "Freeze time must be >= 5 seconds"
        );

        _freezeTime = _value;
        return true;
    }

    function setPercent(uint256 _value) public onlyRole(ADMIN) returns (bool success) {
        require(
            _value == 5 || _value == 10 || _value == 15,
            "Try 5%, 10% or 15%"
        );

        _percent = _value; // percents
        return true;
    }
}