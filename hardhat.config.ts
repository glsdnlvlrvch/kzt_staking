import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";

import "./tasks/KZT/accounts.ts";
import "./tasks/KZT/approve.ts";
import "./tasks/KZT/balance.ts";
import "./tasks/KZT/transfer.ts";
import "./tasks/KZT/transferFrom.ts";

import "./tasks/Staking/stake.ts";
import "./tasks/Staking/claim.ts";
import "./tasks/Staking/reward.ts";
import "./tasks/Staking/unstake.ts";

dotenv.config();

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.0",
      },
      {
        version: "0.5.16",
        settings: {},
      },
    ],
  },
  networks: {
    rinkeby: {
      url: process.env.RINKEBY_URL || "",
      accounts:
        process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [],
    },
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  },
};

export default config;
