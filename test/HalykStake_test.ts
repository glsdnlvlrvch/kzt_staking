import { expect } from "chai";
import { ethers } from "hardhat";

import { Contract, Signer } from "ethers";



describe("HalykStaking", function () {;
    let HalykContract: Contract;
    let rewardContract: Contract;
    let stakingContract: Contract;

    beforeEach(async function () {
        let Factory1 = await ethers.getContractFactory("TokenERC20");
        rewardContract = await Factory1.deploy();
        await rewardContract.deployed();

        let Factory2 = await ethers.getContractFactory("VRToken");
        stakingContract = await Factory2.deploy();
        await stakingContract.deployed();

        let Factory3 = await ethers.getContractFactory("HalykBankStaking");
        HalykContract = await Factory3.deploy(
            rewardContract.address, 
            stakingContract.address, 
            10, 
            10
        );
        await HalykContract.deployed();

        const supply = await rewardContract.totalSupply();
        await rewardContract.transfer(HalykContract.address, supply.div(5).mul(2)) // 40% tokens for staking
    });


    describe("Functions for administator", function () {
        it("setFreezeTime(int256 _value)", async function () {
            await expect(
                HalykContract.setFreezeTime(3)
            ).to.be.revertedWith("Freeze time must be >= 5 seconds");
        
            await expect(
                HalykContract.setFreezeTime(10000)
            ).to.be.revertedWith("Freeze time must be <= 1 hour");

            await HalykContract.setFreezeTime(10);

            expect(
                await HalykContract.freezeTime()
            ).to.be.eq(10);
        });

        it("setPercent(int16 _value)", async function () {
            await expect(
                HalykContract.setPercent(0)
            ).to.be.revertedWith("Try 5%, 10% or 15%");

            
            await HalykContract.setPercent(5)

            expect(
                await HalykContract.percent()
            ).to.be.eq(5);
        });
    });

    describe("Functions for contributors", function () {
        function sleep(milliseconds: Number) {
            const date = Date.now();
            let currentDate = null;
            do {
              currentDate = Date.now();
            } while (currentDate - date < milliseconds);
        }
        

        it("stake(uint256 _value) && claim()", async function () {
            const [owner, signer1] = await ethers.getSigners();

            let value = await stakingContract.balanceOf(
                owner.address
            );

            await stakingContract.transfer(signer1.address, value.div(100));
            
            await stakingContract.connect(signer1).approve(
                HalykContract.address,
                value.div(100)
            )

            // balance equals 0
            expect(
                await rewardContract.balanceOf(signer1.address)
            ).to.be.eq(0);
            
            // FIRST DEPOSIT
            await HalykContract.connect(signer1).stake(value.div(200));
            sleep(11000);

            await HalykContract.connect(signer1).claim();

            expect(
                await rewardContract.balanceOf(signer1.address)
            ).to.be.eq(value.div(4000));

            expect(
                await HalykContract.getContributorReward(signer1.address)
            ).to.be.eq(0);

            // SECOND DEPOSIT
            await HalykContract.connect(signer1).stake(value.div(200));

            expect(
                await HalykContract.getContributorDeposit(signer1.address)
            ).to.be.eq(value.div(100));

            sleep(11000);

            expect(
                await rewardContract.balanceOf(signer1.address)
            ).to.be.eq(value.div(4000));

            await HalykContract.connect(signer1).claim();
            
            expect(
                await rewardContract.balanceOf(signer1.address)
            ).to.be.eq(value.div(4000).mul(3));

            // EXCEPTION
            await stakingContract.transfer(signer1.address, value.div(100));
            
            await stakingContract.connect(signer1).approve(
                HalykContract.address,
                value.div(100)
            )

            await HalykContract.connect(signer1).stake(value.div(200));

            await expect(
                HalykContract.connect(signer1).claim()
            ).to.be.revertedWith("try later")
        })

        it("unstake()", async function () {
            const [owner, signer1] = await ethers.getSigners();
            let value = await stakingContract.balanceOf(
                owner.address
            );
            
            await stakingContract.transfer(signer1.address, value.div(100));
            
            await stakingContract.connect(signer1).approve(
                HalykContract.address,
                value.div(100)
            )

            await expect(
                HalykContract.connect(signer1).unstake()
            ).to.be.revertedWith("nothing to unstake");
        });
    })
});