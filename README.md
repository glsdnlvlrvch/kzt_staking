# :space_invader: KZT token ERC20
Basic implementation of ERC20 token standart with full-coveraged tests and staking contract </br> **KZT ETHERSCAN:** https://rinkeby.etherscan.io/address/0x9EAE6C6C5f107D9919B6f82669F2529E28C8EDF2#code </br> **Staking ETHERSCAN:** https://rinkeby.etherscan.io/address/0x7e4251723037d0ad8E74Bc1659e184b37976FD27#code </br>
## **Features**</br>
 + Burnable</br>
 + Mintable

## Tasks
- balance (check balance by address)
- transfer (transfer KZT to account by address)
- approve (to allow account to spend KZT of sender)
- transferFrom (to transfer KZT from spender to recipient by sender)
- accounts (standart task to get accounts)
- reward (send KZT to staking contract for rewards)
- stake (stake amount of LP tokens)
- claim (get rewards in KZT)
- unstake(get LP tokens back without reward)
